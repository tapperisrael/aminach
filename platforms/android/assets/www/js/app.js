// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.factories','ngCordova','xml','starter.services'])



.run(function($ionicPlatform,$rootScope,SendGetRequestServer,SendPostRequestServer) {
	
	
   //$rootScope.LaravelHost = "http://tapper.co.il/camera-app/laravel/public/";
   //$rootScope.PHPHost = "http://tapper.co.il/camera-app/php/";
   
   //set on login..
   $rootScope.APIUrl = "http://82.166.81.109:9897/Bookeep.asmx/";
   //$rootScope.uploadAPI = "http://82.166.81.109:8098/Service1.svc/";
   $rootScope.showMenuButton = true;

	
  $ionicPlatform.ready(function() {
	  
	  

	  
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	
	

	
	
	
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider,$httpProvider) {
	//$httpProvider.interceptors.push('xmlHttpInterceptor');
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
	
	
    //$httpProvider.defaults.useXDomain = true;

    //Remove the header containing XMLHttpRequest used to identify ajax call 
    //that would prevent CORS from working
    //delete $httpProvider.defaults.headers.common['X-Requested-With'];

	
	
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })


    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })

    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })
	
    .state('app.scan', {
      url: '/scan',
      views: {
        'menuContent': {
          templateUrl: 'templates/scan.html',
          controller: 'ScanCtrl'
        }
      }
    })	

    .state('app.report', {
      url: '/report',
      views: {
        'menuContent': {
          templateUrl: 'templates/report.html',
          controller: 'ReportCtrl'
        }
      }
    })	
	
    .state('app.update_info', {
      url: '/update_info',
      views: {
        'menuContent': {
          templateUrl: 'templates/update_info.html',
          controller: 'UpdateInfoCtrl'
        }
      }
    })		
	
	;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
